package com.cuccorese.galaxy.exceptions;

import com.cuccorese.galaxy.util.errors.GeneralErrors;

/**
 * Throws the exception when missing planets in the galaxy
 *
 * @author Ezequiel Cuccorese <ezequiel.cuccorese@gmail.com>
 *
 */
public class MissingPlanetsException extends ApiException {

    public MissingPlanetsException() {
        super(GeneralErrors.MISSING_PLANETS, null);
    }
}
