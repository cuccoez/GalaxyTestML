/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cuccorese.galaxy.exceptions;

import com.cuccorese.galaxy.util.errors.GeneralErrors;

/**
 * Throws the exception when the galaxy not found.
 *
 * @author Ezequiel Cuccorese <ezequiel.cuccorese@gmail.com>
 */
public class GalaxyNotFoundException extends ApiException {

    private static final long serialVersionUID = 2549832339682820467L;

    public GalaxyNotFoundException() {
        super(GeneralErrors.MISSING_GALAXY, null);
    }
}
