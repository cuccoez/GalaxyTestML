package com.cuccorese.galaxy.repository;

import com.cuccorese.galaxy.domain.ForecastHistory;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

/**
 * Repository for ForecastHistory
 *
 * @author Ezequiel Cuccorese <ezequiel.cuccorese@gmail.com>
 */
public interface ForecastHistoryRepository extends CrudRepository<ForecastHistory, Long> {

    /**
     * Find a forecast day history.
     *
     * @param galaxy    id galaxy.
     * @param numberDay number day.
     *
     * @return the forecast in de day.
     */
    ForecastHistory findByGalaxyIdAndDayNumberEquals(Long galaxy, Integer numberDay);

    /**
     * Find all forecast history.
     *
     * @param id the galaxy ID.
     *
     * @return A list with all forecast.
     */
    List<ForecastHistory> findByGalaxyId(Long id);
    
    @Query("select f from ForecastHistory f "
            + "where galaxy.id = :id and "
            + "perimetre = (select coalesce(max(perimetre), 0) from  ForecastHistory) "
            + "and perimetre != :perimetre")
    List<ForecastHistory> findMaxPerimetres(@Param("id") Long id,@Param("perimetre") Double perimeter);
}
