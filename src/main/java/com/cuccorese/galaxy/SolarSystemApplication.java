package com.cuccorese.galaxy;

import com.cuccorese.galaxy.config.Messages;
import com.cuccorese.galaxy.domain.Coordinate;
import com.cuccorese.galaxy.service.GalaxySystemService;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Principal class. Start the app in the spring container.
 *
 * @author Ezequiel Cuccorese <ezequiel.cuccorese@gmail.com>
 */
@EnableScheduling
@SpringBootApplication
@EntityScan(basePackages = {"com.cuccorese.galaxy.domain"})
@EnableJpaRepositories(basePackages = {"com.cuccorese.galaxy.repository"})
public class SolarSystemApplication {

    private static final Logger LOGGER = LoggerFactory.getLogger(SolarSystemApplication.class);

    /**
     * The start point of the app.
     *
     * @param args
     */
    public static void main(String[] args) {
        SpringApplication.run(SolarSystemApplication.class, args);
    }

    /**
     * Runner for the first data in the application. Based of the guide.
     *
     * @param service  service of galaxy.
     * @param messages message util.
     *
     * @return continue the app startup.
     */
    @Bean
    @Transactional
    public CommandLineRunner demo(GalaxySystemService service, Messages messages) {
        return (args) -> {
            LOGGER.info(messages.get("carga.inicial"));
            service.createGalaxy("Galaxia Mercadera", "SunStart", new Coordinate(0.0, 0.0));
            //El planeta Ferengi se desplaza con una velocidad angular de 1 grados/dia en sentido
            //horario. Su distancia con respecto al sol es de 500Km.
            service.addPlanet(-1L, "Ferengi", 500.0, 1.0, Boolean.TRUE);

            //El planeta Vulcano se desplaza con una velocidad angular de 5 grados / día 
            //en sentido anti­horario, su distancia con respecto al sol es de 1000Km.
            service.addPlanet(-1L, "Vulcano", 1000.0, 5.0, Boolean.FALSE);

            //El planeta Betasoide se desplaza con una velocidad angular de 3 grados / día en sentido horario. 
            //Su distancia con respecto al sol es de 2000Km.
            service.addPlanet(-1L, "Betasoide", 2000.0, 4.0, Boolean.TRUE);

        };
    }
}
