package com.cuccorese.galaxy.util.errors;

/**
 * Defined the error types.
 *
 * @author Ezequiel Cuccorese <ezequiel.cuccorese@gmail.com>
 */
public enum ErrorType {
    GENERAL_ERROR,
    SYSTEM_ERROR,
    BUSINESS_ERROR;
}
