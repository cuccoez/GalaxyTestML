package com.cuccorese.galaxy.util.errors;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;

import com.cuccorese.galaxy.exceptions.ApiException;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Any {@link ApiException} is translated to this class in order to be serialized and reported to the client
 *
 * @author Ezequiel
 *
 */
@Data
@ToString
@EqualsAndHashCode(of = {"status", "path", "errorType"})
@JsonInclude(value = Include.NON_NULL)
public class RestError {

    /**
     * The HTTP status code reported to the client as a result of the operation
     */
    @JsonSerialize(using = HttpStatusCodeSerializer.class)
    public HttpStatus status;

    /**
     * Why the API call failed
     */
    private ErrorType errorType;

    private String path;

    private Date time;

    /**
     * A verbose description of the problem
     */
    private String description;

    public RestError(HttpStatus status, ErrorType errorType, String exceptionDescription, String apiErrorDescription,
            Date time, String path) {
        this.status = status;
        this.errorType = errorType;
        this.description = StringUtils.isNotEmpty(exceptionDescription) ?
                exceptionDescription + " - " + apiErrorDescription : apiErrorDescription;
        this.time = time;
        this.path = path;
    }

}
