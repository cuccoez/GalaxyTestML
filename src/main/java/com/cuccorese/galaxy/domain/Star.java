package com.cuccorese.galaxy.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import lombok.Data;

/**
 * This model defined a general Star for the galaxy.
 *
 * @author Ezequiel Cuccorese <ezequiel.cuccorese@gmail.com>
 *
 */
@Entity
@Data
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class Star implements Serializable {

    @Id
    @GeneratedValue
    @JsonIgnore(true)
    private Long id;
    /**
     * Name of {@link Star}.
     */
    private String name;

    /**
     * Coordinate point of position the {@link Star}.
     */
    @OneToOne(cascade = {CascadeType.ALL})
    private Coordinate coordinate;

    public Star() {
    }

    public Star(String name, Coordinate coordinate) {
        this.name = name;
        this.coordinate = coordinate;
    }

}
