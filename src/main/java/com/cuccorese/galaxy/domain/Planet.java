package com.cuccorese.galaxy.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import javax.persistence.Entity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * This model described a planet. Inherited from an Star.
 *
 * @author Ezequiel Cuccorese <ezequiel.cuccorese@gmail.com>
 *
 */
@Data
@Entity
@EqualsAndHashCode(callSuper = false)
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class Planet extends Star {

    /**
     * It is used to measure the distance of the planet to the sun (radio of orbit). This distance is set at the
     * creation of the planet and can not be modified.
     */
    private Double distanceToSun;

    /**
     * Constant angular velocity with which the planet moves by day. It does not vary in this sunar system.
     */
    private Double angularVelocity;

    /**
     * Direction of rotation of the planet. True: Schedule, False: Counterclockwise.
     */
    private Boolean rotation;

    /**
     * Actual position in degree.
     */
    private Double anglePosition;

    public Planet() {
    }

    public Planet(String name, Double distanceToSun, Coordinate coordinates, Double angularVelocity, Boolean rotation) {
        super(name, coordinates);
        this.setDistanceToSun(distanceToSun);
        this.setAngularVelocity(angularVelocity);
        this.setRotation(rotation);
        this.anglePosition = this.getRotation() ? 0.0 : 365.0;
    }

}
