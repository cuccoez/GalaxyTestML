package com.cuccorese.galaxy.domain;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Enumerates the possible states of time.
 *
 * @author Ezequiel Cuccorese <ezequiel.cuccorese@gmail.com>
 */
public enum Weather {
    DROUGHT("Sequia", "Los planets alineados juntos al sun"),
    RAINS("Lluvia", "Triangulo entre los planets y el sun al medio"),
    HEAVY_RAINS("Lluvias Intensas", "Triangulo entre los planets, el sun al medio y el perimetro es el mayor"),
    OPTIMAL_CONDITIONS("Condiciones optimas", " Los 3 planets alineados entre si, pero no con el sun");

    private Weather(String weather, String description) {
        this.description = description;
        this.weather = weather;
    }

    String description;
    String weather;

    @JsonValue
    public String getWeather() {
        return this.weather;
    }

    public String getDescription() {
        return this.description;
    }
}
